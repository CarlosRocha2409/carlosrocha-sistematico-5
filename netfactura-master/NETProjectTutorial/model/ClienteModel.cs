﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListClientes = new List<Cliente>();

        public static List<Cliente> GetListEmpleado()
        {
            return ListClientes;
        }

        public static void Populate()
        {
            Cliente[] Clientes =
            {
                new Cliente(1, "001-260201-1025V", "Pepito", "Pérez","81615721", "Barcelona200@gmail.com", "Del arbolito 2c. abajo"),
                new Cliente(2, "001-260201-1025F", "Ana", "Conda", "83264848", "ElMeroMero33@hotmail.com", "Del arbolito 2c. abajo" ),
                new Cliente(3, "001-260201-1025J", "Juan", "Camaney","85987654", "MayorgaMatador42@gmail.com", "Del arbolito 2c. abajo")

            };

            ListClientes.AddRange(Clientes);

        }
    }
}
