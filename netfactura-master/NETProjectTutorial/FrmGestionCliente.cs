﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsClientes;
        private BindingSource bsClientes;

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        private void txtFinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsClientes.Filter = string.Format("Cedula like '*{0}*' or Nombre like '*{0}*' or Apellido like '*{0}*'", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
            dgvCliente.DataSource = bsClientes;
            dgvCliente.AutoGenerateColumns = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblClientes = DsClientes.Tables["Cliente"];
            fc.DsClientes = DsClientes;
            fc.ShowDialog();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.TblClientes = DsClientes.Tables["Cliente"];
            fc.DsClientes = DsClientes;
            fc.DrClientes = drow;
            fc.ShowDialog();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsClientes.Tables["Cliente"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
