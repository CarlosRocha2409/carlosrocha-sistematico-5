﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {

        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;

        public DataTable TblClientes
        {
            get
            {
                return tblClientes;
            }

            set
            {
                tblClientes = value;
            }
        }

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        public DataRow DrClientes
        {
            set
            {
                drClientes = value;
                msktxtCedula.Text = drClientes["Cedula"].ToString();
                txtNombre.Text = drClientes["Nombre"].ToString();
                txtApellido.Text = drClientes["Apellido"].ToString();
                msktxtTel.Text = drClientes["Telefono"].ToString();
                txtCorreo.Text= drClientes["Correo"].ToString();
                txtDireccion.Text = drClientes["Direccion"].ToString();
                
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string Cedula, Nombre, Apellido, Telefono, Correo, direccion;
            Cedula = msktxtCedula.Text;
            Nombre = txtNombre.Text;
            Apellido = txtApellido.Text;
            Telefono = msktxtTel.Text;
            Correo = txtCorreo.Text;
            direccion = txtDireccion.Text;

            if (drClientes !=null)
            {
                DataRow drNew = TblClientes.NewRow();

                int index = TblClientes.Rows.IndexOf(drClientes);
                drNew["Id"] = drClientes["Id"];
                drNew["Cedula"] = Cedula;
                drNew["Nombre"] = Nombre;
                drNew["Apellido"] = Apellido;
                drNew["Correo"] = Correo;
                drNew["Telefono"] = Telefono;
                drNew["Direccion"] = direccion;


                TblClientes.Rows.RemoveAt(index);
                TblClientes.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblClientes.Rows.Add(TblClientes.Rows.Count + 1, Cedula, Nombre, Apellido, Telefono, direccion,Nombre+" "+Apellido);
            }

            Dispose();
        }


        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

    }
}
